A short script to access how long it takes for the spdns system to
update the IPv6 records of an associated subdomain and how often the
provided give new IPv6 addresses.

# Usage

Install dependencies.

``` bash
go get github.com/miekg/dns
```

Run script.

``` bash
go run spdnsUpdateCheck.go &> spdns.log
```
